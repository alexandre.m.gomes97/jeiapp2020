package com.example.jeiutad20.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toolbar;

import com.example.jeiutad20.R;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

public class SobreNosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Element btnBack = new Element();
        btnBack.setIconDrawable(R.drawable.ic_arrow_back_black_24dp)
        .setTitle("Voltar")
        .setGravity(Gravity.START)
        .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SobreNosActivity.super.onBackPressed();
            }
        });


        View aboutPage = new AboutPage(this)

                .setImage(R.drawable.logoneei)
                .setDescription("O Núcleo de Estudantes de Engenharia Informática, é a estrutura representativa dos estudantes inscritos no curso de Engenharia Informática da Universidade de Trás-os-Montes e Alto Douro.\n" +
                        "\n" +
                        "O NEEI é o órgão responsável pela organização de atividades culturais, desportivas e recreativas. Deste modo, são realizadas atividades como por exemplo o clube de programação, jornadas, torneios de futebol, torneios de poker e de jogos eletrónicos. Somos também responsáveis pela barraca de curso nas Semanas Académicas, Caloirada aos Montes, no Carnaval e na Mostra do Caloiro. Além disso somos responsáveis pela “barraquinha de Engenharia Informática” que durante o ano  se encontra nas diversas casas da noite de Vila Real (do Grupo BB). Palestras, workshops, encontros nacionais de estudantes de engenharia informática (ENEI), são outras das atividades já realizadas por este núcleo, que pretende sempre continuar a realização e melhoria das mesmas durante os anos.")
                .addGroup("Fale conosco")
                .addEmail("neei@utad.pt")
                .addWebsite("http://neei.utad.pt")
                .addFacebook("neei.utad","Visite-nos no Facebook")
                //.addPlayStore("")
                .addInstagram("neei.utad")
                .addItem(btnBack)
                .create();

        setContentView(aboutPage);
    }
}
