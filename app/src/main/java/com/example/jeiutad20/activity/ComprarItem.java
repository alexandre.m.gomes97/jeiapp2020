package com.example.jeiutad20.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jeiutad20.R;
import com.example.jeiutad20.model.ShopItem;

public class ComprarItem extends Activity {

    private TextView textViewTitulo;
    private TextView textViewDescricao;
    private TextView textViewPreco;
    private EditText editTextQuantidade;
    private Button btnComprar;
    private Button btnFechar;
    private ShopItem shopItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprar_item);
        textViewTitulo = findViewById(R.id.textViewTituloComprar);
        textViewDescricao = findViewById(R.id.textViewDescricaoComprar);
        textViewPreco = findViewById(R.id.textViewPrecoComprar);
        editTextQuantidade = findViewById(R.id.editTextQuantidade);
        btnFechar = findViewById(R.id.btnFechar);

        //Buscar os dados vindos do fragment
        Bundle dados = getIntent().getExtras();
        shopItem = (ShopItem) dados.getSerializable("item") ;

        textViewTitulo.setText(shopItem.getTitulo());
        textViewDescricao.setText(shopItem.getDescricao());
        textViewPreco.setText(shopItem.getPreco()+ " JEIPoints");
        editTextQuantidade.addTextChangedListener(textWatcher);
        btnFechar.setOnClickListener(fecharClickListerner);

        // Alterar as dimensoes da Activity
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.5));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if(!charSequence.toString().equals("")){
                CalcularPreco();
            }else{
                textViewPreco.setText(shopItem.getPreco() + " JEIPoints");
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void CalcularPreco(){
        int quantidade = Integer.parseInt(editTextQuantidade.getText().toString());
        int precoFinal = shopItem.getPreco() * quantidade;
        textViewPreco.setText(precoFinal + " JEIPoints");

    }

    private View.OnClickListener fecharClickListerner
        = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ComprarItem.super.onBackPressed();
        }
    };
}
