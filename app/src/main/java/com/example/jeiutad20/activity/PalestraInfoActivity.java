package com.example.jeiutad20.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;

import com.example.jeiutad20.R;

public class PalestraInfoActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palestra_info);
        toolbar =  findViewById(R.id.toolbarPalestra);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
            toolbar.setNavigationOnClickListener(clickToolbarListener);
        }
    }

    private  View.OnClickListener clickToolbarListener
            = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            PalestraInfoActivity.super.onBackPressed();

        }
    };
}
