package com.example.jeiutad20.activity;

import android.content.Intent;
import android.os.Bundle;

import com.example.jeiutad20.R;
import com.example.jeiutad20.fragment.CalendarioFragment;
import com.example.jeiutad20.fragment.HomeFragment;
import com.example.jeiutad20.fragment.LojaFragment;
import com.example.jeiutad20.fragment.SponsorsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private BottomNavigationView navView;
    private SponsorsFragment sponsorsFragment;
    private LojaFragment lojaFragment;
    private HomeFragment homeFragment;
    private CalendarioFragment calendarioFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Buscar os IDs
        navView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        sponsorsFragment = new SponsorsFragment();
        homeFragment = new HomeFragment();
        lojaFragment = new LojaFragment();
        calendarioFragment = new CalendarioFragment();

        setSupportActionBar(toolbar);
        toolbar.setOnMenuItemClickListener(onMenuToolBarItemClickListener);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.getMenu().findItem(R.id.navigation_home).setChecked(true);

    }

    private  Toolbar.OnMenuItemClickListener onMenuToolBarItemClickListener
            = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){
                case R.id.sobre_nos:
                    startActivity(new Intent(getApplicationContext(),SobreNosActivity.class));

            }
            return false;
        }
    };

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    changeToolBar(R.string.title_home,R.drawable.ic_home_black_24dp,true);
                    transaction.replace(R.id.frameLayout,homeFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_calendario:
                    changeToolBar(R.string.title_calendario,R.drawable.ic_action_name,false);
                    transaction.replace(R.id.frameLayout,calendarioFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_loja:
                    changeToolBar(R.string.title_loja,R.drawable.ic_shopping_cart_black_24dp,false);
                    transaction.replace(R.id.frameLayout,lojaFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_sponsors:
                    changeToolBar(R.string.sponsors,R.drawable.ic_business_black_24dp,false);
                    transaction.replace(R.id.frameLayout,sponsorsFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_codeBattle:
                    changeToolBar(R.string.code_battle,R.drawable.ic_code_black_24dp,false);
                    transaction.replace(R.id.frameLayout,homeFragment);
                    transaction.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        changeToolBar(R.string.title_home,R.drawable.ic_home_black_24dp,true);
        return super.onCreateOptionsMenu(menu);
    }

    private void changeToolBar(int idTitle,int idIcon,boolean overFlowMenu ){
        toolbar.setTitle(idTitle);
        toolbar.setLogo(idIcon);
        toolbar.getMenu().findItem(R.id.edit_profile).setVisible(overFlowMenu);
        toolbar.getMenu().findItem(R.id.sobre_nos).setVisible(overFlowMenu);
    }
}
