package com.example.jeiutad20.model;

public class PalestraWorkshop {
    private String titulo,local,descricao;
    int hora;

    public PalestraWorkshop(String titulo, String local, String descricao,int hora) {
        this.titulo = titulo;
        this.local = local;
        this.descricao = descricao;
        this.hora = hora;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }
}
