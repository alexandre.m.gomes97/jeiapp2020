package com.example.jeiutad20.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.jeiutad20.fragment.SponsorPageFragment;
import com.example.jeiutad20.fragment.SponsorsFragment;

import java.util.List;

public class fragmentSponsorsPagerAdapter extends FragmentPagerAdapter {
    private List<SponsorPageFragment> sponsorPageFragmentsList;

    public fragmentSponsorsPagerAdapter(FragmentManager fm, List<SponsorPageFragment> sponsorPageFragmentsList){
        super(fm);
        this.sponsorPageFragmentsList = sponsorPageFragmentsList;
    }

    @Override
    public Fragment getItem(int position) {
        return sponsorPageFragmentsList.get(position);
    }

    @Override
    public int getCount() {
        return sponsorPageFragmentsList.size();
    }
}
