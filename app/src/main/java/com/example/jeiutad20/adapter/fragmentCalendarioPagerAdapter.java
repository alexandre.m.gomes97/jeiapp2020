package com.example.jeiutad20.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.jeiutad20.fragment.DiaCalendarioFragment;

import java.util.List;

public class fragmentCalendarioPagerAdapter extends FragmentPagerAdapter {

    private DiaCalendarioFragment quaFragment, quiFragment, sexFragment;
    private List<DiaCalendarioFragment> fragmentCalendarioPagerAdapterList;
    private String[] tabTitles = {"Ter", "Qua", "Qui"};

    public fragmentCalendarioPagerAdapter(FragmentManager fm, List<DiaCalendarioFragment> fragmentCalendarioPagerAdapterList) {
        super(fm);
        this.fragmentCalendarioPagerAdapterList = fragmentCalendarioPagerAdapterList;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragmentCalendarioPagerAdapterList.get(position);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return this.tabTitles[position];
    }
}


