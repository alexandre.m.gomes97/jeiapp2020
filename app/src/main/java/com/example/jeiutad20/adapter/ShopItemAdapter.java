package com.example.jeiutad20.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jeiutad20.R;
import com.example.jeiutad20.model.ShopItem;

import java.util.List;

public class ShopItemAdapter extends RecyclerView.Adapter<ShopItemAdapter.ShopITemViewHolder>  {

    private OnItemListerner onItemListerner;
    private List<ShopItem>  itemsList;

    public ShopItemAdapter(OnItemListerner onItemListerner,List<ShopItem> _itemsList)
    {
        this.onItemListerner = onItemListerner;
        this.itemsList = _itemsList;
    }

    @NonNull
    @Override
    public ShopITemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_cardview,parent,false);
        return new ShopITemViewHolder(viewLista,onItemListerner);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopITemViewHolder holder, int position) {
        holder.imageView.setImageResource(itemsList.get(position).getImagem());
        holder.textViewTitulo.setText(itemsList.get(position).getTitulo());
        holder.textViewDescricao.setText(itemsList.get(position).getDescricao());
        holder.textViewPreco.setText(itemsList.get(position).getPreco() + "JEIPoints");
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class ShopITemViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        private OnItemListerner onItemListerner;

        private ImageView imageView;
        private TextView textViewTitulo;
        private TextView textViewDescricao;
        private TextView textViewPreco;
        private CardView click;

        public ShopITemViewHolder(@NonNull View itemView,OnItemListerner _onItemListerner) {
            super(itemView);
            // Todas as views do cardView_ShopItem tem ShopItem no ID
            this.imageView = itemView.findViewById(R.id.imageViewShopItem);
            this.textViewTitulo = itemView.findViewById(R.id.textViewShopItemTitulo);
            this.textViewDescricao = itemView.findViewById(R.id.textViewShopItemDescricao);
            this.textViewPreco = itemView.findViewById(R.id.textViewShopItemPreco);

            this.onItemListerner = _onItemListerner;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            onItemListerner.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemListerner{
        void onItemClick(int position);
    }
}
