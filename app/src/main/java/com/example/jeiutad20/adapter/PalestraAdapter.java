package com.example.jeiutad20.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jeiutad20.R;
import com.example.jeiutad20.fragment.DiaCalendarioFragment;
import com.example.jeiutad20.model.PalestraWorkshop;

import java.util.List;

public class PalestraAdapter extends RecyclerView.Adapter<PalestraAdapter.PalestraViewHolder> {

    private List<PalestraWorkshop> diaCalendarioList;
    private OnItemListerner onItemListerner;

    public PalestraAdapter(List<PalestraWorkshop> diaCalendarioList,OnItemListerner onItemListerner){
        this.diaCalendarioList = diaCalendarioList;
        this.onItemListerner = onItemListerner;

    }

    @NonNull
    @Override
    public PalestraViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.palestra_workshop_view,parent,false);
        return new PalestraViewHolder(viewLista,this.onItemListerner);
    }

    @Override
    public void onBindViewHolder(@NonNull PalestraViewHolder holder, int position) {
        holder.titulo.setText(diaCalendarioList.get(position).getTitulo());
        holder.descricao.setText(diaCalendarioList.get(position).getDescricao());
        int hora = diaCalendarioList.get(position).getHora();
        String sHora = (hora < 10) ? "0" + String.valueOf(hora) + "H" : String.valueOf(hora) + "H";
        holder.hora.setText(sHora);
        holder.local.setText(diaCalendarioList.get(position).getLocal());
    }

    @Override
    public int getItemCount() {
        return diaCalendarioList.size();
    }

    public class PalestraViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private  OnItemListerner onItemListerner;
        private CardView cardView;
        private TextView hora,titulo,local,descricao;

        public PalestraViewHolder(@NonNull View itemView,OnItemListerner _onItemClickListerner) {
            super(itemView);
            this.hora=itemView.findViewById(R.id.textViewPalestraHora);
            this.local=itemView.findViewById(R.id.textViewPalestraLocal);
            this.titulo=itemView.findViewById(R.id.textViewPalestraTitulo);
            this.descricao=itemView.findViewById(R.id.textViewPalestraDes);
            this.cardView = itemView.findViewById(R.id.cardViewPalestra);
            this.onItemListerner = _onItemClickListerner;
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) { onItemListerner.onItemClick(getAdapterPosition()); }
    }
    public interface OnItemListerner{
        void onItemClick(int position);
    }
}
