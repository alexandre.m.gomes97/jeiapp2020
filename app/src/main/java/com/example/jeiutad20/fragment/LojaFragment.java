package com.example.jeiutad20.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeiutad20.R;
import com.example.jeiutad20.activity.ComprarItem;
import com.example.jeiutad20.adapter.ShopItemAdapter;
import com.example.jeiutad20.model.ShopItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LojaFragment extends Fragment implements ShopItemAdapter.OnItemListerner {

    private List<ShopItem> itemList;
    private RecyclerView recyclerView;


    public LojaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_loja, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewShop);

        itemList = new ArrayList<ShopItem>();
        loadShopItemList();

        //Definir Adapatador
        ShopItemAdapter shopItemAdapter = new ShopItemAdapter(this,itemList);
        recyclerView.setAdapter(shopItemAdapter);

        //Definir layout
        LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
        linearManager.setOrientation(RecyclerView.VERTICAL);

        recyclerView.setLayoutManager(linearManager);
        recyclerView.setFocusable(false);

        return view;
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getContext(),ComprarItem.class);
        intent.putExtra("item",itemList.get(position));
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        recyclerView.smoothScrollToPosition(0);
    }


    public void loadShopItemList(){
        for(int i = 0; i< 10; i++){
            itemList.add(new ShopItem("Coisas"+i,"Descrição"+i,100+(i*100),R.drawable.logoneei));
        }

    }
}
