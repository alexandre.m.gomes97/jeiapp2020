package com.example.jeiutad20.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeiutad20.R;
import com.example.jeiutad20.adapter.fragmentSponsorsPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SponsorsFragment extends Fragment {

    private ViewPager viewPager;
    private List<SponsorPageFragment> sponsorPageFragmentsList;

    public SponsorsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_sponsors, container, false);
        viewPager = view.findViewById(R.id.viewPagerSponsors);
        sponsorPageFragmentsList = new ArrayList<SponsorPageFragment>();
        this.fillSponsorsList();

        //Definir Adapatador
        viewPager.setAdapter(new fragmentSponsorsPagerAdapter(getChildFragmentManager(),sponsorPageFragmentsList));
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                sponsorPageFragmentsList.get(position).setScrollUp();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        viewPager.setCurrentItem(0);
    }

    public void fillSponsorsList(){
        for(int i = 0; i < 5; i++)
            sponsorPageFragmentsList.add(new SponsorPageFragment());
    }

}
