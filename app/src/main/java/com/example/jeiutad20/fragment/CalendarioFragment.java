package com.example.jeiutad20.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jeiutad20.R;
import com.example.jeiutad20.adapter.fragmentCalendarioPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarioFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView textViewTitulos;
    private String[] temas = {"Tema1","Tema2","Tema3"};
    private List<DiaCalendarioFragment> fragmentCalendarioPagerAdapterList;

    public CalendarioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_calendario, container, false);
        textViewTitulos = view.findViewById(R.id.textViewCalendarioTemas);
        tabLayout = view.findViewById(R.id.tabLayoutCalendario);
        viewPager = view.findViewById(R.id.viewPagerCalendario);

        fragmentCalendarioPagerAdapterList = new ArrayList<DiaCalendarioFragment>();
        fragmentCalendarioPagerAdapterList.add(new DiaCalendarioFragment());
        fragmentCalendarioPagerAdapterList.add(new DiaCalendarioFragment());
        fragmentCalendarioPagerAdapterList.add(new DiaCalendarioFragment());

        // Definir adaptador
        viewPager.setAdapter(new fragmentCalendarioPagerAdapter(getChildFragmentManager(),fragmentCalendarioPagerAdapterList));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(tabSelectedListener);

        return view;
    }

    private OnTabSelectedListener tabSelectedListener
            = new OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            viewPager.setCurrentItem(tab.getPosition());
            textViewTitulos.setText(temas[tab.getPosition()]);
            try{
                fragmentCalendarioPagerAdapterList.get(tab.getPosition()).recyclerViewPushUp();
            }catch (NullPointerException npe){}

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

   @Override
   public void onResume() {
        super.onResume();
        viewPager.setCurrentItem(0);
        tabLayout.setupWithViewPager(viewPager);
    }

}
