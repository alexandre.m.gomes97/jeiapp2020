package com.example.jeiutad20.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.jeiutad20.R;
import com.example.jeiutad20.activity.ComprarItem;
import com.example.jeiutad20.activity.PalestraInfoActivity;
import com.example.jeiutad20.adapter.PalestraAdapter;
import com.example.jeiutad20.adapter.ShopItemAdapter;
import com.example.jeiutad20.model.PalestraWorkshop;
import com.example.jeiutad20.model.ShopItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiaCalendarioFragment extends Fragment implements PalestraAdapter.OnItemListerner {

    private RecyclerView recyclerView;
    private List<PalestraWorkshop> palestraWorkshopList;

    public DiaCalendarioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_dia_calendario, container, false);
        recyclerView = view.findViewById(R.id.recycleViewDiaCalendario);
        palestraWorkshopList = new ArrayList<PalestraWorkshop>();
        loadListOfTheDay();

        //Definir Adapatador
        PalestraAdapter palestraAdapter = new PalestraAdapter(palestraWorkshopList,this);
        recyclerView.setAdapter(palestraAdapter);

        //Definir layout
        LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
        linearManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearManager);
        recyclerView.smoothScrollToPosition(0);
        return view;
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getContext(), PalestraInfoActivity.class);
        startActivity(intent);
    }

    public void recyclerViewPushUp(){
        recyclerView.getLayoutManager().scrollToPosition(0);
    }

    public void loadListOfTheDay(){
        for(int i=0; i < 6; i++){
            palestraWorkshopList.add(new PalestraWorkshop("Palestra"+i,"Local"+i,"Desicaoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"+i,i+5));
        }

    }

}
