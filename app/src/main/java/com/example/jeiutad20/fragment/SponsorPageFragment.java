package com.example.jeiutad20.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.example.jeiutad20.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SponsorPageFragment extends Fragment {
    private ScrollView scrollView;

    public SponsorPageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.sponsors_cardview, container, false);
        scrollView = view.findViewById(R.id.scrollViewSponsors);
        scrollView.setFocusable(false);
        return  view;

    }

    public void setScrollUp(){
        scrollView.smoothScrollTo(0,0);
    }
}
